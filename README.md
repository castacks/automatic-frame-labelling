
## Auto Labeling a Video
In `main.py`, change the dataFile variable to point to the correct day folder. It will make a folder for each video within the day and auto label each video. It will skip over any videos that have issues with the ADS-B data.

## Splitting Video Into Images
Navigate into the cam1 folder for a video sequence and run `ffmpeg -i ../1_1.MP4 -r 10/1 %d.png`. Then navigate to the cam2 folder and run `ffmpeg -i ../1_2.MP4 -r 10/1 %d.png`.

Once the images have been created, there are usually more than there are for the .label files. To remove the extras, run `filter_frames.py`. Right now you need to change the file paths to the correct folder. *Todo: make function*

## Hand Labeling
Run `labeler.py`. Select Offline

## Azure Python API support
Step 1. Add variable export AZURE_STORAGE="<Connection key1>"

Step 2. Run `labeler.py`.

Step 3. Select Online, add container and path inside container.

Step 4. Open the same folder (to save label files) after mouting it using blobfuse on the local machine.