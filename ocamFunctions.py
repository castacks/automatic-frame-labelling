import numpy as np
import os

################################################################################

#Ocam_Calib's Functions, implemented using numpy for matrix interface

#Original matlab code is included in block comments for reference and debugging
#Orginal matrices are replaced with numpy arrays, so note differences

#Original Code by Professor Davide Scaramuzza

################################################################################

#read the ocamModel parameters from the calib_results.txt file exported by
#ocam_calib tool after calibration
def readCalib(dataFile, cameraNum):
    calibName = "calib_results_" + str(cameraNum) + ".txt"
    calibFile = os.path.join(dataFile, calibName)
    f = open(os.path.normpath(calibFile), "rt") 
    rawFile = f.read()
    lines = rawFile.split("\n")
    polyCoef = lines[2] #string with all polynomial coefficients
    center = lines[10]  #string with row, col (ocam maps to xc, yc)
    affine = lines[14]  #affine parameter string
    dims = lines[18]    #height, width string 

    #ocam_model data structure:
    # map of variable float values to variable string keys
    ocamModel = dict()
    ocamModel["ss"]     = polyCoef.split(" ")[1:-1]
    for i in range(len(ocamModel["ss"])):
        elem = ocamModel["ss"][i]
        ocamModel["ss"][i] = float(elem)
    centerCoords = center.split(" ")
    ocamModel["xc"]     = float(centerCoords[0]) + 1 #row +1 for convention
    ocamModel["yc"]     = float(centerCoords[1]) + 1 #col
    cde = affine.split(" ")
    ocamModel["c"]      = float(cde[0])
    ocamModel["d"]      = float(cde[1])
    ocamModel["e"]      = float(cde[2])
    dimList = dims.split(" ")
    ocamModel["height"] = float(dimList[0]) #height
    ocamModel["width"]  = float(dimList[1]) #width

    return ocamModel

#assemble the 3xN array of 3D points derived using affine parameters
def getpoint(ss, m):
    w = np.concatenate(m[1,:],
                       m[2,:],
                       np.polyval(ss[::-1],np.sqrt(m[1,:]**2 + m[2,:]**2)))

'''
function w=getpoint(ss,m)

% Given an image point it returns the 3D coordinates of its correspondent
% optical ray

w = [m(1,:) ; m(2,:) ; polyval(ss(end:-1:1),sqrt(m(1,:).^2+m(2,:).^2)) ];
'''

#project points from  2D image plane to its camera's world coordinate frame
def cam2world(m, ocamModel):
    n_points = size(m,1)

    ss = ocamModel["ss"] #array with the polynomial coeff. in reverse order
    xc = ocamModel["xc"]
    yc = ocamModel["yc"]
    width = ocamModel["width"]
    height = ocamModel["height"]
    c = ocamModel["c"]
    d = ocamModel["d"]
    e = ocamModel["e"]

    A = np.array([c, d], [e, 1])
    T = np.array([xc], [yc]) @ ones((1,n_points))

    m = inv(A) @ (m - T)
    M = getpoint(ss, m)
    M = M / np.linalg.norm(M, axis = 0)
'''
function M=cam2world(m, ocam_model)

n_points = size(m,2);

ss = ocam_model.ss;
xc = ocam_model.xc;
yc = ocam_model.yc;
width = ocam_model.width;
height = ocam_model.height;
c = ocam_model.c;
d = ocam_model.d;
e = ocam_model.e;

A = [c,d;
     e,1];
T = [xc;yc]*ones(1,n_points);

m = A^-1*(m-T);
M = getpoint(ss,m);
%normalizes coordinates so that they have unit length
M = normc(M);

'''

#caliculate ideal undistorted 2D row cols from 3D coordinates
def omni3d2pixels(ss, M, width, height):
    #set all 0's in (0,0,z) to minimum value to avoid div by 0
    zeroIndices = np.logical_and(M[0,:] == 0, M[1,:] == 0)
    M[0, zeroIndices] = np.spacing(1)
    M[1, zeroIndices] = np.spacing(1)

    #imagine a ray projecting out from the camera center
    #draw a normal line from the ray to a real-world point
    #m is the ratio of the ray length to the line length
    m = M[2,:]/(np.sqrt(M[0,:]**2 + M[1,:]**2))

    length = m.size
    rho = np.zeros(length)
    poly_coef = ss[::-1] #make sure this is an array of appropriate proportion
    poly_coef_tmp = poly_coef.copy()

    for j in range(length):
        poly_coef_tmp[-2] = poly_coef[-2] - m[j]
        rhoTmp = np.roots(poly_coef_tmp)
        #we want the smallest, real non-zero root of the polynomial
        #complex component must be zero, real must be +
        res = rhoTmp[np.angle(rhoTmp) == 0]
        #filter out 0 edge case
        res = res[res != 0]
        if res.size == 0:
            rho[j] = 0
        elif res.size > 1:
            rho[j] = res.min()
        else:
            rho[j] = float(res[0])

    x = M[0,:]/np.sqrt(M[0,:]**2 + M[1,:]**2)*rho
    y = M[1,:]/np.sqrt(M[0,:]**2 + M[1,:]**2)*rho
    return x, y


'''
function [x,y] = omni3d2pixel(ss, xx, width, height)


%convert 3D coordinates vector into 2D pixel coordinates

%These three lines overcome problem when xx = [0,0,+-1]
ind0 = find((xx(1,:)==0 & xx(2,:)==0)); %no zero opposite values
xx(1,ind0) = eps;
xx(2,ind0) = eps;

%adjacent over opposite, 
%if we imagine a triangle from origin to a point, base on outward ray
m = xx(3,:)./sqrt(xx(1,:).^2+xx(2,:).^2); 

rho=[];
poly_coef = ss(end:-1:1); %reverse ss for the coefficients of the polynomial
poly_coef_tmp = poly_coef;
for j = 1:length(m)
    %subtract each triangle ratio from the first degree coeff. of the polynomial
    poly_coef_tmp(end-1) = poly_coef(end-1)-m(j); 
    rhoTmp = roots(poly_coef_tmp);
    res = rhoTmp(find(imag(rhoTmp)==0 & rhoTmp>0));% & rhoTmp<height )); %obrand
    if isempty(res) %| length(res)>1    %obrand
        rho(j) = NaN;
    elseif length(res)>1    %obrand
        rho(j) = min(res);    %obrand
    else
        %rho is the smallest root of the polynomial with new coefficients
        rho(j) = res;
    end
end
%x and y have a hypotenuse that is rho
x = xx(1,:)./sqrt(xx(1,:).^2+xx(2,:).^2).*rho ;
y = xx(2,:)./sqrt(xx(1,:).^2+xx(2,:).^2).*rho ;

'''

#project points from camera's world coordinate frame to its 2D image plane
def world2cam(M, ocamModel):
    ss = ocamModel["ss"]
    xc = ocamModel["xc"]
    yc = ocamModel["yc"]
    width = ocamModel["width"]
    height = ocamModel["height"]
    c = ocamModel["c"]
    d = ocamModel["d"]
    e = ocamModel["e"]

    (x, y) = omni3d2pixels(ss, M, width, height)
    #apply affine transform to account for camera and mirror misalignment
    m = np.zeros((2,x.size))
    m[0,:] = x*c + y*d + xc 
    m[1,:] = x*e + y   + yc
    return m

'''
function m=world2cam(M, ocam_model)
ss = ocam_model.ss;
xc = ocam_model.xc;
yc = ocam_model.yc;
width = ocam_model.width;
height = ocam_model.height;
c = ocam_model.c;
d = ocam_model.d;
e = ocam_model.e;

[x,y]  = omni3d2pixel(ss,M,width, height);
m(1,:) = x*c + y*d + xc;
m(2,:) = x*e + y   + yc;
'''