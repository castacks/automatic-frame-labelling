import numpy as np
import math
from ocamFunctions import *
import os

################################################################################

#FAA-Provided Lookup Databases:
#MASTER File: Used for ID
#elements: 0) N-number, 1) serial number, 2) aircraft Mfr Model Code, 
    #          3) Engine Mfr Mode Code, 4) Year Mfr, 5) Type registrant,
    #          6) Registrant's name, 7) Street 1, 8) Street 2, 9) City,
    #          10) State, 11) Zip Code, 12) Region, 13) County Mail,
    #          14) Country Mail, 15) Last Activity Date, Certificate Issue Date,
    #          16) Certification requested and uses,
    #          17) Approved Operation Codes, 18) Type of Aircraft,
    #          19) Type Engine, 20) Status Code, 21) Mode S Code,
    #          22) Fractional Owenership, 23) Airworthiness Data,
    #          24) Other Name 1, 25) Other Name 2, 26) Other Name 3,
    #          27) Other Name 4, 28) Other Name 5, 29) Expiration Date,
    #          30) Unique ID, 31) Kit Mfr, 32) Kit Model, 33) Mode S Code Hex,

#AIRCRAFT REFERENCE File: Used to get mfr and model name
#elements: 0) Code, 1) Mfr, 2) Model, 3) Type-Aircraft, 4) Type-Engine,
    #          5) Aircraft Category Code, 6) Builder's Certification Code, 
    #          7) Number of Engines, 8) Number of Seats, 9) Aircraft Weight,
    #          10) Aircraft Cruising Speed, 

#Aircraft Characteristics Database (posted 10/5/2018)
#Source: https://www.faa.gov/airports/engineering/aircraft_char_database/
#elements: 0) Date Completed, 1) Manufacturer, 2) Model,
#          3) Physical Class (Engine), 4)# Engines, 5) AAC, 6) ADG, 7) TDG,
#          8) Approach Speed, 9) Wingtip Config, 10) Wingspan, 11) Length,
#          12) Tail Height, 13) Wheelbase, 14) Cockpit to Main Gear,
#          15) MGW, 16) MTOW, 17) Max Ramp Max Taxi, 18) Main Gear Config,
#          19) ICAO Code, 20) Wake Category, 21) ATCT Weight Class,
#          22) Years Manufactured, 23) Note, 24) Parking Area
################################################################################

def initDetectionDict():
    detectionDict = dict()
    detectionDict["Birds"] =            "ba" 
    detectionDict["Fixed Wing UAVs"] =  "fa" 
    detectionDict["Rotor UAVs"] =       "ra"
    detectionDict["GA Planes"] =        "ga"
    detectionDict["CA Planes"] =        "ca"
    detectionDict["Helos"] =            "ha"
    detectionDict["Ultra Lights"] =     "ma"
    detectionDict["Balloons"] =         "oa"
    detectionDict["Blimps"] =           "pa"
    detectionDict["UFO"] =              "ua"
    detectionDict["Military"] =         "aa"
    return detectionDict

def initTypeDict():
    typeDict = dict()
    typeDict["1"] = ["Glider", "GA Planes"]
    typeDict["2"] = ["Balloon", "Balloons"]
    typeDict["3"] = ["Blimp", "Blimps"]
    typeDict["4"] = ["Fixed wing single engine", "GA Planes"]
    typeDict["5"] = ["Fixed wing multi engine", "CA Planes"]
    typeDict["6"] = ["Rotorcraft", "Helos"]
    typeDict["7"] = ["Weight-shift-control", "Ultra Lights"]
    typeDict["8"] = ["Powered Parachute", "Ultra Lights"]
    typeDict["9"] = ["Gyroplane", "Ultra Lights"]
    typeDict["H"] = ["Hybrid Lift", "Blimps"]
    typeDict["O"] = ["Other", "UFO"]
    return typeDict

def initEngineDict():
    engineDict = dict()
    engineDict["0"] = ["None", "GA Planes"]
    engineDict["1"] = ["Reciprocating", "GA Planes"]
    engineDict["2"] = ["Turbo-prop", "GA Planes"]
    engineDict["3"] = ["Turbo-shaft", "CA Planes"]
    engineDict["4"] = ["Turbo-jet", "CA Planes"]
    engineDict["5"] = ["Turbo-fan", "CA Planes"]
    engineDict["6"] = ["Ramjet", "CA Planes"]
    engineDict["7"] = ["2 Cycle", "GA Planes"]
    engineDict["8"] = ["4 Cycle", "GA Planes"]
    engineDict["9"] = ["Unknown", "GA Planes"]
    engineDict["10"] = ["Electric", "GA Planes"]
    engineDict["11"] = ["Rotary", "h"]
    return engineDict

#change to dictionary implementation for search and access efficiency
def extractFileData(filename):
    if filename.endswith(".txt"):
        AircraftDatabase = open(filename, "r")
    #reads each line off of the FAA database, stores as a list of strings
        dataList = AircraftDatabase.readlines() 
    elif filename.endswith(".csv"):
        with open(os.path.normpath(filename), "rt") as f:
            csvData = f.read()
        dataList = csvData.split("\n")

    for i in range(len(dataList)):
        line = dataList[i]
        dataList[i] = line.split(",")
        for j in range(len(dataList[i])):
            element = dataList[i][j]
            dataList[i][j] = element.strip()
            #makes 2D list. First dimension: line. Second: Element
    
    return dataList

#Look for the aircraft in the FAA database based on the Mode 2 code
def lookForPlane(masterFile, manufactureRefFile, ID, engineDict, typeDict,
                 detectionDict):
    #first check the ID, corresponds to the Mode S Code Hex
    typeLetter = ""
    manufacturer = ""
    modelName = ""
    weightClass = ""
    print(ID + "/")
    for aircraft in masterFile:
        #print(aircraft[0])
        if aircraft[0] == ID: #if we get an Mode S Code match...
            aircraftType = aircraft[18] #get the aircraft type
            if aircraftType == "4" or aircraftType == "5":
                engineType = aircraft[19]
                typeName = (engineDict[engineType])[1]
            else:
                typeName = (typeDict[aircraftType])[1]
            typeLetter = detectionDict[typeName]

            manufactureCode = aircraft[2]
            
            for model in manufactureRefFile:
                code = model[0]
                if code == manufactureCode:
                    manufacturer = model[1]
                    modelName = model[2]
                    weightClass = int(model[9].replace("CLASS ", ""))

            print("Hit!" + ID)
            return (typeLetter, manufacturer, modelName, weightClass)

    if typeLetter == "": typeLetter = "u"
    return (typeLetter, manufacturer, modelName, weightClass)

#provide the bbox sizes for the aircraft at each frame, returns as dictionary
def getBoxSize(FAAData, ocamModel):
    width = ocamModel["width"]
    height = ocamModel["height"]
    #reference areas for aircraft of each weight class in km^2
    weightDict = {1:0.0004, 2:0.0016, 3:0.0256, 4:0.0001}
    #eg class 1 is 20mx20m box

    #look-up dictionaries for our classification conventions
    detectionDict = initDetectionDict()
    typeDict = initTypeDict()
    engineDict = initEngineDict()

    #Edit here to change location of the reference files
    masterFile = extractFileData("MASTER.txt")
    manufactureRefFile = extractFileData("ACFTREF.txt")
    #characteristicsFile = extractFileData(
    #                      "FAA-Aircraft-Char-Database-v2-201810.csv")

    bboxDict = dict()
    for aircraft in FAAData:
        points = FAAData[aircraft]
        rangeArr = np.linalg.norm(points, axis = 0)
        length = points.shape[1]
        tail = aircraft[1:]

        result = lookForPlane(masterFile, manufactureRefFile, tail,
                              engineDict, typeDict, detectionDict)
        (typeLetter, manufacturer, modelName, weightClass) = result
        '''
        #Attempt at looking up characteristics directly
        if (manufacturer != "" and modelName != ""):
            planeDims= lookForDims(manufacturer, modelName, characteristicsFile)
        else:
            planeDims = None
        '''
        if weightClass == "":
            typeSize = 0.005776 #in km^2, 76m x 76m target (Boeing 747)
        else:
            typeSize = weightDict[weightClass]

        totalSurface = 2 * math.pi * (rangeArr ** 2)
        ratio = np.sqrt(typeSize / totalSurface)
        ratio[rangeArr >= 1.5] = 0 #aircraft beyond 1.5 km have a 0 length bbox
        boxSizes = np.array([ratio * width, ratio * height])
        bboxDict[aircraft] = (boxSizes, typeLetter)

    return bboxDict

def withinEdge(x, y, xc, yc, radius):
    dx = x - xc
    dy = y - yc
    return math.sqrt(dx ** 2 + dy ** 2) <= radius

def makeLabels(rot, FAAData, boxSizes, ocamModel, dataFile, camera, writeFile):
    labelDict = dict()
    xlen = ocamModel["width"]
    ylen = ocamModel["height"]
    xc = ocamModel["yc"] + 1 #model format: yc = center col = center x coord
    yc = ocamModel["xc"] + 1 #xc = center row = center y coord
    radius = min(xlen - xc, ylen - yc, xc, yc)

    for aircraft in FAAData:
        positions = FAAData[aircraft]
        length = positions.shape[1]
        camPos = rot @ positions
        #make necessary switches for camera 3D frame
        camPos = np.vstack((-camPos[2,:],
                            camPos[0,:],
                            -camPos[1,:]))
        M = world2cam(camPos, ocamModel)
        (boxes, label) = boxSizes[aircraft]
        #write all the label files
        for i in range(length):
            xSize = boxes[0, i]
            ySize = boxes[1, i]
            xPos = M[1,i]
            yPos = M[0,i]
            
            if xSize != 0 and ySize != 0: #we only want to draw bboxes when in range
                xMin = round(xPos - xSize / 2)
                yMin = round(yPos - ySize / 2)
                xMax = round(xPos + xSize / 2)
                yMax = round(yPos + ySize / 2)
                
                #Concept: if a bbox is beyond a certain radius from cam center
                #consider it as out of frame. This frame is the smallest
                #distance from center to the edge of the valid picture.
                # In other words, the valid picture is slightly eliptical.
                # This function captures a circle strictly within that ellipse.
                # There may still be visible aircraft excluded from this radius.
                # Will need to fix this.
                if withinEdge(xMin, yMin, xc, yc, radius) and \
                withinEdge(xMin, yMax, xc, yc, radius) and \
                withinEdge(xMax, yMin, xc, yc, radius) and \
                withinEdge(xMax, yMax, xc, yc, radius):
                    labelName =  str(i + 1) + ".label"
                    fileName = os.path.join(dataFile, camera, labelName)
                    if writeFile:
                        if os.path.isfile(fileName):
                            permission = "a"
                        else:
                            permission = "w"
                        
                        labelFile = open(fileName, permission)
                        labelFile.write(f"%d %d %d %d %s%s"
                                        % (xMin, yMin, xMax, yMax, label,"\n"))
                        labelFile.close()

                    if aircraft in labelDict:
                        np.concatenate((labelDict[aircraft], 
                                        np.array([[xMin],
                                                [yMin],
                                                [xMax],
                                                [yMax]])), axis = 1)
                    else:
                        labelDict[aircraft] = np.array([[xMin],
                                                        [yMin],
                                                        [xMax],
                                                        [yMax]])
    return labelDict

#rot is rotation matrix
#FAAData is the dictionary containing the positions for each aircraft
def labelVid(rot, FAAData, ocamModel, dataFile, camera, writeFile = False):
    boxSizes = getBoxSize(FAAData, ocamModel)
    camDataPath = os.path.join(dataFile, camera)
    if not os.path.isdir(camDataPath):
        os.mkdir(camDataPath) #prepare the data directory
    labels = makeLabels(rot, FAAData, boxSizes, ocamModel, dataFile, camera, writeFile)
    return labels
