#!/usr/bin/python3
import os, uuid,io
import random
import sys
import cv2
import time
import numpy as np
from interp_labels import atoi, natural_keys, interpolate_labels
from PyQt5.QtCore import Qt, QRect, QPoint, QSize
from PyQt5.QtWidgets import (
    QApplication,
    QWidget,
    QMainWindow,
    QMenuBar,
    QAction,
    QLabel,
    QVBoxLayout,
    QFileDialog,
    QInputDialog,
)
from PyQt5.QtGui import QPixmap, QPainter, QIcon, QPen, QImage, QCursor
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient, __version__
from azure_api_test import get_azure

current_milli_time = lambda: int(round(time.time() * 1000))

class ImageView(QWidget):
    def __init__(self):
        super().__init__()
        
        self.dx = 0
        self.dy = 0
        self.imageScale = 1.0

        self.setGeometry(40, 40, 40, 40)
        self.setMouseTracking(True)

        self.initVars()
        self.initUI()
        
        self.needsTreackerReInit = True

   

    def initVars(self):
        self.waitingForBBox = False

        self.currentlyRectDrawing = False
        self.currentRectBase = None

        self.boundginBoxes = []
        self.currentBoundingBox = None

        self.labels = []
        self.boundingBoxLabel = None

        self.imageRes = (2496,2496)
        self.systemRes = (1900,920)

    def initUI(self):
        vbox = QVBoxLayout(self)

        self.label = QLabel()
        self.updateImageFromPath(
            "/home/lorenz/dev/cmu/work/mbzirc/chlg1/labeler/test.png"
        )

        self.resize(self.pixmap.width(), self.pixmap.height())

        vbox.addWidget(self.label)

    def startRectDrawing(self, event):
        self.currentlyRectDrawing = True
        self.currentRectBase = self.label.mapFromGlobal(event.globalPos())
        orgSize = self.pixmap.size()
        currY = self.systemRes[1]
        diff = (orgSize.height() - currY) / 2
        self.currentBoundingBox = [
            ((self.currentRectBase.x() - self.dx) * self.imageScale,
             (self.currentRectBase.y() + diff - self.dy) * self.imageScale),
            None,
        ]

    def drawRect(self, event):
        orgSize = self.pixmap.size()
        currY = self.systemRes[1]
        diff = (orgSize.height() - currY) / 2

        currentPoint = self.label.mapFromGlobal(event.globalPos())
        currentX = int((currentPoint.x())) #- self.dx) * self.imageScale)
        currentY = int((currentPoint.y()) + diff) #- self.dy + diff) * self.imageScale)
        botRight = QPoint(currentX, currentY)
        

        prevX = int((self.currentRectBase.x())) # - self.dx) * self.imageScale)
        prevY = int((self.currentRectBase.y()) + diff) # - self.dy + diff) * self.imageScale)
        topLeft = QPoint(prevX, prevY)
        rect = QRect(topLeft, botRight)
        print(prevX, prevY)

        self.canvas = self.pixmap.copy()

        painter = QPainter(self.canvas)
        painter.setPen(QPen(Qt.red, 1, Qt.SolidLine))
        painter.drawRect(rect)
        painter.drawText(topLeft, self.boundingBoxLabel)

        self.label.setPixmap(self.canvas)
        self.update()

    def endRectDrawing(self, event):
        currentPoint = self.label.mapFromGlobal(event.globalPos())
        orgSize = self.pixmap.size()
        currY = self.systemRes[1]
        diff = (orgSize.height() - currY) / 2

        self.currentBoundingBox[1] = ((currentPoint.x() - self.dx) * self.imageScale,
                                      (currentPoint.y() + diff - self.dy) * self.imageScale)
        self.boundginBoxes.append(self.currentBoundingBox)
        self.labels.append(self.boundingBoxLabel)

        self.currentBoundingBox = None
        self.boundingBoxLabel = None
        self.currentlyRectDrawing = False
        self.waitingForBBox = False

        self.finalize()

    def deleteRectHovering(self): #going to previous may mess this up. Suspect it is because when old frames are loaded, the bounding boxes are not updated to old ones
        cursorPos = self.label.mapFromGlobal(QCursor().pos())
        orgSize = self.pixmap.size()
        currY = self.systemRes[1]
        diff = (orgSize.height() - currY) / 2

        indexes = []
        for index, box in enumerate(self.boundginBoxes):
            if (
                cursorPos.x() > ((box[0][0]) / self.imageScale + self.dx)
                and cursorPos.x() < ((box[1][0]) / self.imageScale + self.dx)
                and cursorPos.y() > ((box[0][1]) / self.imageScale + self.dy - diff)
                and cursorPos.y() < ((box[1][1]) / self.imageScale + self.dy - diff)
            ):
                indexes.append(index)

        for index in indexes:
            self.boundginBoxes[index] = None
            self.labels[index] = None

        self.boundginBoxes = [i for i in self.boundginBoxes if i is not None]
        self.labels = [i for i in self.labels if i is not None]

        self.finalize()

    def finalize(self):
        self.pixmap = self.pixmapBase.copy()
        self.pixmap = self.pixmap.scaled(self.imageRes[0]/self.imageScale,
                                         self.imageRes[1]/self.imageScale)
        self.pixmap.scroll(int(self.dx), int(self.dy), self.pixmap.rect())

        if len(self.boundginBoxes) > 0:
            painter = QPainter(self.pixmap)
            painter.setPen(QPen(Qt.blue, 1, Qt.SolidLine))

        for packet in zip(self.boundginBoxes, self.labels):
            box = packet[0]
            label = packet[1]

            p1 = QPoint(int((box[0][0]) / self.imageScale + self.dx),
                        int((box[0][1]) / self.imageScale + self.dy))
            p2 = QPoint(int((box[1][0]) / self.imageScale + self.dx),
                        int((box[1][1]) / self.imageScale + self.dy))

            rect = QRect(p1, p2)

            painter.drawRect(rect)
            painter.drawText(int((box[0][0])/ self.imageScale + self.dx + 10),
                             int((box[0][1])/ self.imageScale + self.dy - 10),
                             label)

        self.label.setPixmap(self.pixmap)
        self.update()

    def mousePressEvent(self, event):
        if (
            event.buttons() == Qt.LeftButton
            and not self.currentlyRectDrawing
            and self.waitingForBBox
        ):
            self.startRectDrawing(event)
            self.needsTreackerReInit = True

    def mouseMoveEvent(self, event):
        # Drawing
        if event.buttons() == Qt.LeftButton and self.currentlyRectDrawing:
            self.drawRect(event)
        cursorPos = self.label.mapFromGlobal(QCursor().pos())
        self.main.statusBar().showMessage("Cursor at {}".format(cursorPos))

    def mouseReleaseEvent(self, event):
        if self.currentlyRectDrawing:
            self.endRectDrawing(event)

    def updateImageFromPath(self, path):
        pixmapBase = QPixmap(path)
        self.updateImageFromPixmap(pixmapBase)

    def updateImageFromPixmap(self, pixmap):
        self.pixmapBase = pixmap
        self.pixmap = self.pixmapBase.copy()
        self.pixmap = self.pixmap.scaled(int(self.imageRes[0]/self.imageScale),
                                         int(self.imageRes[1]/self.imageScale))
        self.pixmap.scroll(self.dx, self.dy, self.pixmap.rect())
        #self.pixmap = self.pixmap.scaled(1920, 1080) #experimental
        self.label.setPixmap(self.pixmap)

        self.label.setFixedSize(QSize(self.systemRes[0], self.systemRes[1]))#self.pixmap.size())
        #print(self.pixmap.size())

        self.update()

        self.cleanupNextFrame()

    def cleanupNextFrame(self):
        self.initVars()


class Labeler(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initVars()
        self.initUI()
        if self.exec_mode == "Online":
            self.initAzure()
        self.show()

################################################################################

#Initialization for Labeler

################################################################################

    def initVars(self):
        self.VIDEO = 1
        self.IMAGES = 2

        self.iv = None

        self.waitingForLabel = False

        self.frameCount = 1
        self.maxFrameCount = 1
        self.targetFrame = 1
        self.currentFrame = None

        self.enableTracking = True
        self.needsTreackerReInit = True
        self.trackingOverrideLabel = False

        self.trackers = []

        # Mode 1 = Video mode
        # Mode 2 = Folder/Image mode
        self.mode = None

        self.videoPath = None
        self.videoCapture = None

        self.imagePath = None

        self.frameScale = 0.5

        self.currentlyActive = False
    
    def initAzure(self):

        connect_str = os.getenv('AZURE_STORAGE')
        # connect_str = "DefaultEndpointsProtocol=https;AccountName=saadatasets;AccountKey=5lF8CldFLlQOk2UXR9TSuL2aqsM2L8/ai32ctL009sOhxy5yks9PyjqpK+WJL6gKlahbEvKXbXyNpapMaQWKJA==;EndpointSuffix=core.windows.net"
        self.blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        
        # self.azure_container_name = "inflight-garmin-data"
        
        # self.azure_dir_path = "processed/1/"

        print("Azure Connected")

    def initUI(self):
        menubar = self.menuBar()

        openVideoAction = QAction("Open Video", self)
        openVideoAction.setShortcut("Ctrl+O")
        openVideoAction.triggered.connect(self.openVideo)

        openFolderAction = QAction("Open Folder", self)
        openFolderAction.setShortcut("Ctrl+Shift+O")
        openFolderAction.triggered.connect(self.openFolder)

        selectNewBoxAction = QAction("New BBox", self)
        selectNewBoxAction.setShortcut("N")
        selectNewBoxAction.triggered.connect(self.getNewBBoxLabel)

        self.currentFrameAction = QAction("Current Frame", self)
        self.currentFrameAction.setShortcut(" ")
        self.currentFrameAction.triggered.connect(self.spaceBarPressed)

        self.nextFrameAction = QAction("Next Frame", self)
        self.nextFrameAction.setShortcut("1")
        self.nextFrameAction.triggered.connect(self.onePressed)

        self.skip10FrameAction = QAction("Skip 10", self)
        self.skip10FrameAction.setShortcut("2")
        self.skip10FrameAction.triggered.connect(self.twoPressed)

        self.skip100FrameAction = QAction("Skip 100", self)
        self.skip100FrameAction.setShortcut("3")
        self.skip100FrameAction.triggered.connect(self.threePressed)

        self.prevFrameAction = QAction("Previous Frame", self)
        self.prevFrameAction.setShortcut("4")
        self.prevFrameAction.triggered.connect(self.fourPressed)

        self.back10FrameAction = QAction("Back 10", self)
        self.back10FrameAction.setShortcut("5")
        self.back10FrameAction.triggered.connect(self.fivePressed)

        self.back100FrameAction = QAction("Back 100", self)
        self.back100FrameAction.setShortcut("6")
        self.back100FrameAction.triggered.connect(self.sixPressed)

        toggleTrackingCheckbox = QAction(
            "Enable Tracking", self, checkable=True
        )
        toggleTrackingCheckbox.setChecked(True)
        toggleTrackingCheckbox.triggered.connect(self.toggleTracking)

        toggleTrackOverrideLabelCheckbox = QAction(
            "Tracking override Labels", self, checkable=True
        )
        toggleTrackOverrideLabelCheckbox.setChecked(False)
        toggleTrackOverrideLabelCheckbox.triggered.connect(self.toggleTrackingOverrideLabels)

        deleteBBoxAction = QAction("Delete BBox", self)
        deleteBBoxAction.setShortcut("d")
        deleteBBoxAction.triggered.connect(self.deleteBBox)

        clearBBoxAction = QAction("Clear BBox", self)
        clearBBoxAction.setShortcut("p")
        clearBBoxAction.triggered.connect(self.clearBBox)

        goToNextUnlabeledAction = QAction("Go to next unlabeled frame", self)
        goToNextUnlabeledAction.setShortcut("Tab")
        goToNextUnlabeledAction.triggered.connect(self.goToNextUnlabeled)

        self.zoomInAction = QAction("In", self)
        self.zoomInAction.setShortcut("=")
        self.zoomInAction.triggered.connect(self.plusPressed)

        self.zoomOutAction = QAction("Out", self)
        self.zoomOutAction.setShortcut("-")
        self.zoomOutAction.triggered.connect(self.minusPressed)

        self.goRightAction = QAction("Right", self)
        self.goRightAction.setShortcut("Right")
        self.goRightAction.triggered.connect(self.rightPressed)

        self.goLeftAction = QAction("Left", self)
        self.goLeftAction.setShortcut("Left")
        self.goLeftAction.triggered.connect(self.leftPressed)

        self.goUpAction = QAction("Up", self)
        self.goUpAction.setShortcut("Up")
        self.goUpAction.triggered.connect(self.upPressed)

        self.goDownAction = QAction("Down", self)
        self.goDownAction.setShortcut("Down")
        self.goDownAction.triggered.connect(self.downPressed)

        self.interpolateFramesAction = QAction("Interpolate Frames", self)
        self.interpolateFramesAction.setShortcut("i")
        self.interpolateFramesAction.triggered.connect(self.interpPressed)

        self.toolbar = self.addToolBar("Control")
        self.toolbar.addAction(selectNewBoxAction)
        self.toolbar.addAction(self.nextFrameAction)
        self.toolbar.addAction(self.skip10FrameAction)
        self.toolbar.addAction(self.skip100FrameAction)
        self.toolbar.addAction(self.prevFrameAction)
        self.toolbar.addAction(self.back10FrameAction)
        self.toolbar.addAction(self.back100FrameAction)
        self.toolbar.addAction(self.currentFrameAction)
        self.toolbar.addAction(self.zoomInAction)
        self.toolbar.addAction(self.zoomOutAction)
        self.toolbar.addAction(self.goRightAction)
        self.toolbar.addAction(self.goLeftAction)
        self.toolbar.addAction(self.goUpAction)
        self.toolbar.addAction(self.goDownAction)
        self.toolbar.addAction(toggleTrackingCheckbox)
        self.toolbar.addAction(toggleTrackOverrideLabelCheckbox)
        self.toolbar.addAction(deleteBBoxAction)
        self.toolbar.addAction(clearBBoxAction)
        self.toolbar.addAction(self.interpolateFramesAction)

        fileMenu = menubar.addMenu("File")
        fileMenu.addAction(openVideoAction)
        fileMenu.addAction(openFolderAction)

        toolsMenu = menubar.addMenu("Tools")
        toolsMenu.addAction(goToNextUnlabeledAction)

        items = ("Offline","Online")
        item, okPressed = QInputDialog.getItem(self, "Get item","Mode:", items, 0, False)
        
        if item == "Offline":
            self.exec_mode = "Offline"
            print("Labeller in Offline mode")
        else:
            self.exec_mode = "Online"
            self.azure_container_name, ok = QInputDialog.getText(self, 'Text Input Dialog', 'Enter your contanier:',text = "inflight-garmin-data" )
            self.azure_dir_path, ok = QInputDialog.getText(self, 'Text Input Dialog', 'Enter your dir:',text = "processed/1/" )

 
            # self.azure_container_name = "inflight-garmin-data"
        
        # self.azure_dir_path = "processed/1/"

        self.iv = ImageView()
        self.setCentralWidget(self.iv)
        self.iv.main = self

################################################################################

#Menu Bar Actions

################################################################################
    def getPlanes(self, imagePath):
        #get the csv file path
        parent = os.path.dirname(imagePath)
        allFiles = os.listdir(parent)
        for item in allFiles:
            if item.endswith(".csv"):
                csv = item
        csvPath = os.path.join(parent, csv)

        #access the csv data, we are interested in the first column, plane names
        with open(os.path.normpath(csvPath), "rt") as f:
            rawData = f.read()
        lines = rawData.split("\n")[1:]
        for i in range(len(lines)):
            lines[i] = lines[i].split(",")
        planes = set()
        for line in lines:
            if len(line) > 2:
                planes.add(line[15])

        #assign keys to each plane
        keys = {",", ".","/",";","'","[","]"} #handle 7 planes for now
        planeDict = dict()
        for plane in planes:
            key = random.sample(keys, 1) #returns a 1-item list of a random key
            while key[0] in planeDict: #make sure key is unused
                key = random.sample(keys, 1)
            planeDict[key[0]] = plane

        #return the key mapping for each aircraft
        return planeDict


    def openFolder(self):
        self.mode = self.IMAGES

        self.imagePath = QFileDialog.getExistingDirectory(
            self, "Open Directory", "/home"
        )

        self.planeKeys = self.getPlanes(self.imagePath)
        dirpath = self.generateDirPath()

        # find the lowest number in frames
        files = list(os.listdir(dirpath))
        self.frameCount = int(min(files, key=lambda x: float((x.split('.')[0]))).split('.')[0])
        self.maxFrameCount = self.frameCount
        self.targetFrame = self.frameCount
        self.loadNextFrameIntoView(self.frameCount)

    def openVideo(self, path=None):
        if not path:
            fileName = QFileDialog.getOpenFileName(self, "Open Video", "~/workspaces/ML_Project/cvpr15/videos")
        else:
            fileName = [path]

        if fileName[0]:
            self.mode = 1
            self.videoPath = fileName[0]
            self.videoCapture = None

            self.loadNextFrameIntoView()

    def checkForExistingLabel(self, frameCount):
        dirPath = self.generateDirPath()
        labelPath = dirPath + "/" + str(frameCount) + ".label" #os.path.join(dirPath, "{}.label".format(frameCount))
        print(labelPath)
        if os.path.isfile(labelPath):
            print("is file!")
            with open(labelPath) as labelFile:
                content = labelFile.read()
                print(content)
                labelsString = content.split("\n")[:-1]
                labels = []
                for label in labelsString:
                    labels.append(label.split(" "))
                print(labels)
                return (True, labels)
        else:
            return (False, None)

    def getNewBBoxLabel(self):
        self.statusBar().showMessage(
            "Please press another key to label the new bounding box!"
        )
        self.waitingForLabel = True
        # print(self.planeKeys)

    def keyPressEvent(self, event):
        if self.waitingForLabel:
            label = event.text()
            if label:
                # if label in self.planeKeys:
                    # label = self.planeKeys[label]
                self.statusBar().showMessage(
                    "Pressed key {}. This is the label for the next BBox.".format(
                        label
                    )
                )#
                self.iv.waitingForBBox = True
                self.iv.boundingBoxLabel = label #
            else:
                self.statusBar().showMessage("Error: Invalid Key")

            self.waitingForLabel = False

    def generateDirPath(self):
        dirname = None

        if self.mode == self.VIDEO:
            dirname = os.path.dirname(os.path.abspath(self.videoPath))
            dirname = os.path.join(
                dirname,
                "{}-{}".format(os.path.basename(self.videoPath), "labels"),
            )
        elif self.mode == self.IMAGES:
            dirname = self.imagePath

        if not os.path.isdir(dirname):
            os.mkdir(dirname)

        return dirname

    def saveLabels(self):
        bboxes = self.iv.boundginBoxes
        labels = self.iv.labels

        filename = os.path.join(
            self.generateDirPath(), "{}.label".format(self.frameCount)
        )
        
        if os.path.isfile(filename):
            os.remove(filename)
        with open(filename, "w") as labelfile:
            for package in zip(bboxes, labels):
                box = package[0]
                label = package[1]

                labelfile.write(
                    "{} {} {} {} {}\n".format(
                        int(box[0][0]),
                        int(box[0][1]),
                        int(box[1][0]),
                        int(box[1][1]),
                        label,
                    )
                )

    def saveImage(self):
        start = int(round(time.time() * 1000))
        filename = os.path.join(
            self.generateDirPath(), "{}.png".format(self.frameCount)
        )
        
        if os.path.isfile(filename):
            os.remove(filename)
        
        cv2.imwrite(filename, self.currentFrame)
        #print("ImWrite Time: {}".format(int(round(time.time() * 1000)) - start))

    def saveImageWithLabels(self):
        filename = os.path.join(
            self.generateDirPath(), "{}-labeled.png".format(self.frameCount)
        )

        bboxes = self.iv.boundginBoxes
        labels = self.iv.labels
        toSave = self.currentFrame.copy()

        for package in zip(bboxes, labels):
            box = package[0]
            label = package[1]

            print("{} {}".format(type(box[0]), type(box[1])))
            # cv2.rectangle(toSave, [int(i) for i in box[0]], [int(i) for i in box[1]], (0, 0, 255), 1)
            cv2.putText(
                toSave, label, box[0], cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255)
            )

        cv2.imwrite(filename, toSave)

################################################################################

#Traversing Frames

################################################################################

    def onePressed(self):
        self.nextFrameAction.triggered.disconnect()
        self.targetFrame += 1
        self.sendToFrame()
        self.nextFrameAction.triggered.connect(self.onePressed)

    def twoPressed(self):
        self.skip10FrameAction.triggered.disconnect()
        self.targetFrame += 10
        self.sendToFrame()
        self.skip10FrameAction.triggered.connect(self.twoPressed)
    
    def threePressed(self):
        self.skip100FrameAction.triggered.disconnect()
        self.targetFrame += 100
        self.sendToFrame()
        self.skip100FrameAction.triggered.connect(self.threePressed)
    
    def fourPressed(self):
        self.prevFrameAction.triggered.disconnect()
        self.targetFrame -= 1
        if self.targetFrame < 1:
            self.targetFrame = 1
        self.sendToFrame()
        self.prevFrameAction.triggered.connect(self.fourPressed)

    def fivePressed(self):
        self.back10FrameAction.triggered.disconnect()
        self.targetFrame -= 10
        if self.targetFrame < 0:
            self.targetFrame = 0
        self.sendToFrame()
        self.back10FrameAction.triggered.connect(self.fivePressed)

    def sixPressed(self):
        self.back100FrameAction.triggered.disconnect()
        self.targetFrame -= 100
        if self.targetFrame < 0:
            self.targetFrame = 0
        self.sendToFrame()
        self.back100FrameAction.triggered.connect(self.sixPressed)

    def spaceBarPressed(self):
        self.currentFrameAction.triggered.disconnect()
        self.frameCount = self.maxFrameCount
        self.sendToFrame()
        self.currentFrameAction.triggered.connect(self.spaceBarPressed)
    
    def plusPressed(self):
        self.zoomInAction.triggered.disconnect()
        self.iv.imageScale = self.iv.imageScale / 2.0
        self.iv.dx = self.iv.dx * 2.0
        self.iv.dy = self.iv.dy * 2.0
        self.iv.finalize()
        self.zoomInAction.triggered.connect(self.plusPressed)
    
    def minusPressed(self):
        self.zoomOutAction.triggered.disconnect()
        self.iv.imageScale = self.iv.imageScale * 2.0
        self.iv.dx = self.iv.dx / 2.0
        self.iv.dy = self.iv.dy / 2.0
        self.iv.finalize()
        self.zoomOutAction.triggered.connect(self.minusPressed)

    def rightPressed(self):
        self.goRightAction.triggered.disconnect()
        self.iv.dx -= 400
        self.iv.finalize()
        print("dx: %f gy: %f" %(self.iv.dx, self.iv.dy))
        self.goRightAction.triggered.connect(self.rightPressed)

    def leftPressed(self):
        self.goLeftAction.triggered.disconnect()
        self.iv.dx += 400
        self.iv.finalize()
        print("dx: %f gy: %f" %(self.iv.dx, self.iv.dy))
        self.goLeftAction.triggered.connect(self.leftPressed)

    def upPressed(self):
        self.goUpAction.triggered.disconnect()
        self.iv.dy += 400
        self.iv.finalize()
        print("dx: %f gy: %f" %(self.iv.dx, self.iv.dy))
        self.goUpAction.triggered.connect(self.upPressed)

    def downPressed(self):
        self.goDownAction.triggered.disconnect()
        self.iv.dy -= 400
        self.iv.finalize()
        print("dx: %f gy: %f" %(self.iv.dx, self.iv.dy))
        self.goDownAction.triggered.connect(self.downPressed)

    def interpPressed(self):
        self.interpolateFramesAction.triggered.disconnect()
        print("Interpolating labels in {}".format(self.generateDirPath()))
        interpolate_labels(self.generateDirPath())
        self.interpolateFramesAction.triggered.connect(self.interpPressed)

    def sendToFrame(self):
        '''
        if self.frameCount < self.targetFrame:
            while (self.frameCount < self.targetFrame):
                self.loadNextFrameIntoView(self.frameCount + 1)
                
        elif self.frameCount >= self.targetFrame:
        '''
        self.loadNextFrameIntoView(self.targetFrame)

    def generateFileName(self):
        rawNum = self.frameCount + 10000 #this is a problem. it needs to be 10000. Try changing all the 0 limits for maxFrameCount
        rawStr = str(rawNum)
        result = rawStr[1:]
        return result

    #
    def loadNextFrameIntoView(self, newFrame):
        print("override2: {}".format(self.trackingOverrideLabel))
        #time = current_milli_time()

        #print("Saving: {}".format(current_milli_time() - time))

        #time = current_milli_time()
        #save them frame you were just on
        if len(self.iv.boundginBoxes) > 0:
            self.saveLabels()
            # self.saveImageWithLabels()
            # self.saveImage()

        # Restart the trackers if needed
        if True:
            currentBBoxes = self.iv.boundginBoxes
            labels = self.iv.labels
            if self.needsTreackerReInit or self.iv.needsTreackerReInit:
                self.trackers = []
                for box in currentBBoxes:
                    t = cv2.TrackerKCF_create()
                    trackerSuccess = t.init(self.currentFrame, self.pointsToRect(box[0], box[1]))
                    if trackerSuccess: print("Tracker was init. with: {}".format(box))
                    #tracker is aware of the frame and the boundaries of the object
                    self.trackers.append(t)
                    
                    ok, bbox = t.update(self.currentFrame)
                    if ok: print("pre-tracking worked")
                    else: print("pre-tracking failed")
                    #HAVE THE TRACKER UPDATE THE POSITION ONCE, SEE IF THIS HELPS

                if self.frameCount > 1: #experimental
                    self.iv.needsTreackerReInit = False
                    self.needsTreackerReInit = False
        
        self.frameCount = newFrame
        
        print("Going into frame {}".format(self.frameCount))
        #print("Tracker Init: {}".format(current_milli_time() - time))

        time = current_milli_time()

        # Display the next frame
        if self.mode == self.VIDEO:
            if self.frameCount == 0:
                self.videoCapture = cv2.VideoCapture(self.videoPath)

            ok, self.currentFrame = self.videoCapture.read()
            if ok:
                self.loadFrameIntoView(self.currentFrame)

        elif self.mode == self.IMAGES: #need to figure out if there is a delay in file naming
            dirpath = self.generateDirPath()

            if self.exec_mode == "Offline":
                filepath = os.path.join(dirpath, "{}.png".format(self.frameCount)) #is 1 actually 2
            #modify here for file name

                self.currentFrame = cv2.imread(filepath)
            else:
                print("Getting frame from azure")
                filepath = os.path.join(self.azure_dir_path, "{}.png".format(self.frameCount)) 
                print(self.azure_container_name,filepath)
                blob_client = self.blob_service_client.get_blob_client(container=self.azure_container_name, blob=filepath)
                data = blob_client.download_blob() 
                ee = io.BytesIO(data.content_as_bytes())
                self.currentFrame = cv2.imdecode(np.asarray(bytearray(ee.read()),dtype=np.uint8),cv2.IMREAD_COLOR)
                print("Got frame from azure")
     
            self.loadFrameIntoView(self.currentFrame)

        existing_labels = self.checkForExistingLabel(self.frameCount)
        existing_labels, do_labels_exist = (
            existing_labels[1],
            existing_labels[0],
        )
        #if labels exist, load them up
        if do_labels_exist:
            self.iv.boundginBoxes = []
            self.iv.labels = []

            for label in existing_labels:
                box = (
                    (int(label[0]), int(label[1])),
                    (int(label[2]), int(label[3])),
                )
                label_char = label[4]

                self.iv.boundginBoxes.append(box)
                self.iv.labels.append(label_char)

            print("Existing boxes: {}".format(self.iv.boundginBoxes))
            self.iv.finalize()

        #print("Display new frame: {}".format(current_milli_time() - time))

        time = current_milli_time()

        #print("Labels exist: {}".format(do_labels_exist))
        #print("Tracking enabled: {}".format(self.enableTracking))
        # Track the balloons if enabled, and no previous labels
        print("Old boxes: {}".format(self.iv.boundginBoxes))
        print("eneable tracking: {}".format(self.enableTracking))
        print("override1: {}".format(self.trackingOverrideLabel))
        if self.enableTracking and not do_labels_exist:
            newBBoxes = []
            for tracker in self.trackers:
                ok, box = tracker.update(self.currentFrame) #tracker looks at new frame, and does positional computation
                if ok:
                    print("Updating bboxes")
                    newBBoxes.append(self.rectToPoints(box)) # somehow change this so that the box is immediately saved, put the save fn first. this way, clicking next means approving the current image.
                else:
                    print("Tracking did not work")
                    self.needsTreackerReInit = True
                    # print('Tracker yieleded the result: {}'.format(box))
            print("New boxes: {}".format(newBBoxes))
            self.iv.labels = labels
            self.iv.boundginBoxes = newBBoxes
            self.iv.finalize()
        elif self.enableTracking and self.trackingOverrideLabel:
            print("Override old label")
            newBBoxes = []
            for tracker in self.trackers:
                ok, box = tracker.update(
                    self.currentFrame)  # tracker looks at new frame, and does positional computation
                if ok:
                    print("Updating bboxes")
                    newBBoxes.append(self.rectToPoints(
                        box))  # somehow change this so that the box is immediately saved, put the save fn first. this way, clicking next means approving the current image.
                else:
                    print("Tracking did not work")
                    self.needsTreackerReInit = True
                    # print('Tracker yieleded the result: {}'.format(box))
            print("New boxes: {}".format(newBBoxes))
            self.iv.labels = labels
            self.iv.boundginBoxes = newBBoxes
            self.iv.finalize()
            # print("Also adding the tracker label")
            # newBBoxes = []
            # for tracker in self.trackers:
            #     ok, box = tracker.update(
            #         self.currentFrame)  # tracker looks at new frame, and does positional computation
            #     if ok:
            #         print("Updating bboxes")
            #         self.iv.boundginBoxes.append(self.rectToPoints(
            #             box))
            #         newBBoxes.append(self.rectToPoints(
            #             box))  # somehow change this so that the box is immediately saved, put the save fn first. this way, clicking next means approving the current image.
            #     else:
            #         print("Tracking did not work")
            #         self.needsTreackerReInit = True
            #         # print('Tracker yieleded the result: {}'.format(box))
            # print("New boxes: {}".format(newBBoxes))
            # self.iv.labels.append(labels)
            # self.iv.finalize()

        #print("Tracking: {}".format(current_milli_time() - time))
        """
        self.needsTreackerReInit = (
            self.iv.needsTreackerReInit or self.needsTreackerReInit
        )
        self.iv.needsTreackerReInit = (
            self.iv.needsTreackerReInit or self.needsTreackerReInit
        )
        """
        if self.frameCount > self.maxFrameCount:
            self.maxFrameCount = self.maxFrameCount + 1

        print("Looking at Frame: {}, to reach {}".format(self.frameCount, self.targetFrame))
        print("Furthest Frame: {}".format(self.maxFrameCount))

    def loadFrameIntoView(self, frame):
        self.currentFrame = frame
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        pixmap = QPixmap.fromImage(
            QImage(
                frame.data, frame.shape[1], frame.shape[0], QImage.Format_RGB888
            )
        )
        self.iv.updateImageFromPixmap(pixmap)

    def deleteBBox(self, frame):
        self.iv.deleteRectHovering()

    def clearBBox(self):
        self.iv.initVars()
        self.iv.finalize()

    def toggleTracking(self, toggleState):
        self.statusBar().showMessage("Tracking set to: {}".format(toggleState))
        self.enableTracking = toggleState
        self.needsTreackerReInit = True

    def toggleTrackingOverrideLabels(self, toggleState):
        self.statusBar().showMessage("Tracking override labels set to: {}".format(toggleState))
        print("override3: {}".format(self.trackingOverrideLabel))
        self.trackingOverrideLabel = toggleState
        print("override4: {}".format(self.trackingOverrideLabel))

    def goToNextUnlabeled(self):
        while self.checkForExistingLabel(self.frameCount)[0]:
            self.frameCount = self.frameCount + 1
        self.loadNextFrameIntoView()

    def pointsToRect(self, p1, p2):
        x = min([p1[0], p2[0]])
        y = min([p1[1], p2[1]])

        w1 = int(abs(x - p1[0]))
        w = w1 if w1 != 0 else int(abs(x - p2[0]))

        h1 = int(abs(y - p1[1]))
        h = h1 if h1 != 0 else int(abs(y - p2[1]))

        return (x, y, w, h)

    def rectToPoints(self, rect):
        p1 = (rect[0], rect[1])
        p2 = (p1[0] + rect[2], p1[1] + rect[3])

        return (p1, p2)


if __name__ == "__main__":
 
    container_name = "inflight-garmin-data"
    local_file_name = "processed/1/2.png"
    data = get_azure(container_name,local_file_name)
    app = QApplication(sys.argv)
    l = Labeler()
    sys.exit(app.exec_())