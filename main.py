from ocamFunctions import *
from Script import linearizeADSBData
import os
import numpy as np
import argparse
from labelVid import labelVid
import shutil

def getFAAData():
    allFiles = os.path.listdir()

#gets the data recording number, x.txt, x.csv, x_1.MP4 and x_2.MP4 should all return x
def getNumber(pathName):
    pathName = pathName.replace(".csv", "")
    pathName = pathName.replace(".txt", "")
    pathName = pathName.replace(".MP4", "")
    pathName = pathName.replace("_1", "")
    pathName = pathName.replace("_2", "")
    return pathName

#organizes a directory with raw video, csv, and txt files into directories with
#recordings and corresponding csv and txt
def arrangeData(dataFile):
    data = os.listdir(dataFile)
    numFiles = len(data)
    for i in range(numFiles):
        item = data[i]
        itemPath = os.path.join(dataFile, item)
        if not os.path.isdir(itemPath):
            number = getNumber(item)
            dataDir = os.path.join(dataFile, number)
            if not os.path.isdir(dataDir):
                os.makedirs(dataDir, exist_ok=True)
            shutil.move(itemPath, dataDir)
        
dataFile = os.path.join(os.pardir, "09-06-20")

arrangeData(dataFile)
#provide the location details. May want to pass these to the function in the
# future, either as args or read from directory
cameraHeight = 375 #303
camLocation = (40.777886, -79.949893) #Butler Airport
#(40.430306, -79.924497) #GPS of Jay's house
frameRate = 10

#Based on far points
rot = [np.array([[0.2199, 0.9755, 0.0003],
                [-0.9749, 0.2197, 0.0348],
                [0.0339, -0.0079, 0.9994]]) ,
    np.array([[-0.2138, -0.9769, -0.0017],
                [0.9761, -0.2136, -0.0404],
                [0.0399, -0.0070, 0.9992]]) ] #corrected altitude
print(os.listdir(dataFile))
for folder in os.listdir(dataFile):
    print("\nProcessing folder " + folder)
    dataGroup = os.path.join(dataFile, folder)
    if os.path.isdir(os.path.join(dataGroup,'cam1')) and os.path.isdir(os.path.join(dataGroup,'cam2')):
        print("Already processed this file, skipping")
        continue
    # if folder != "1":
    #     break
    #make imgtxt files with all the positions for a given frame
    try:
        FAAData = linearizeADSBData(dataGroup, camLocation, cameraHeight, frameRate)
    except:
        print("Error in linearizeADSBData")
        continue
    # break
    for camera in range(2):
        
        #Calibration:
        #read calibration results
        calibModel = readCalib(os.curdir, camera + 1)

        camName = "cam" + str(camera + 1)
        #Labeling:
        # apply the yaw to those positions
        labels = labelVid(rot[camera], FAAData, calibModel, dataGroup, camName, True)
        # decide which camera's frame to use
        # then project to the camera frame
        # find the bounding box size
        # find place the bounding box on the frame
