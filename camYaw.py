import os
import math

"""
Algorithmic Framework:
Goal: Find the difference between camera yaw and a North bearing
What we have:
Manual labelling-produced bboxes
ADS-B Data on XYZ position of aircraft

Assume that the camera's center is true north
    - Use cam2world to find the real world xyz of the aircraft as they pass through the image
    - What about the stitching together?
    - For now, try to get data on what the tool can, that means allowing the stitched data to corrupt...
    - that's not gonna work...we don't know the camera center anymore

Find out the plane's course based on this assumption
    -

Compare this course to the true course, as given by ADS-B

Difference is the yaw
"""

"""
Find the difference between real-world xyz on unit sphere and projected unit sphere
    Find the real-world xyz
        Collect script.py output (x, y, z in the real world). Project onto unit sphere
    Find the projected xyz
        Find camera XY
            loop through labels
            find the centers of the bounding box
        Give to cam to world to get xyz on unit sphere

Lines 42, 78, 107, 117
"""
def normalize(x, y, z)
    mag = math.sqrt(x^2 + y^2 + z^2)
    return (x/mag, y/mag, z/mag)

#get the real positions of the aircraft
def getRealXYZ(FAAPath):
    #read the output of script.py
    result = dict()
    files = os.listdir(FAAPath)
    files.remove("General.txt")

    for doc in files:
        #get the xyz
        content = readFile(os.path.join(FAAPath, doc))
        lines = content.split("\n")
        cartesian = lines[6] 
        XYZ = cartesian.split(",")
        (x, y, z) = (float(XYZ[0]), float(XYZ[1]), float(XYZ[2]))
        xyz = normalize(x, y, z) #normalize x,y,z to the unit sphere

        #get the time
        #timeInfo = lines[0].split(" ")
        #time = timeInfo[3].split(":")
        #triplet = (hour, minute, second) = (int(time[0]), int(time[1]), float(time[2]))
        
        frame = doc[4:-4] #take off "file" and ".txt" to get frame number as str

        #create a dictionary: keys - frames and values - xyz
        result[frame] = xyz
        
    """
    content = readFile(os.path.join(FAAPath, "General.txt"))
    timeLine = (content.split("\n"))[1]
    timeList = timeLine.split(":")
    hour = int(timeList[0])
    minute = int(timeList[1])
    second = float(timeList[2])
    startTime = (hour, minute, second)
    """
    return result, startTime #return dictionary

def getCamXYZ(csvFile):
    #Read output of convert_to_csv.py
    result = dict()
    rawCSV = readFile(csvFile)
    lines = rawCSV.split("\n")
    lines.pop(0) #remove the headers
    for line in lines:
        #get the bounding box corners
        columns = line.split(",")
        xMin = int(columns[5])
        yMin = int(columns[6])
        xMax = int(columns[7])
        yMax = int(columns[8])
        frame = columns[1][:-4]

        #find the center (x,y)
        xMid = int((xMin + xMax) / 2)
        yMid = int((yMin + yMax) / 2)
        center = (xMid, yMid)

        #cam to world on the (x,y), get (x,y,z)
        (x,y,z) = cam2World() #not possible in Python (maybe?)

        #create a dictionary keys: times, values: (x, y, z)

        result[frame] = (x, y, z)
        #return dictionary
    return result

def findDiff(realXYZ, camXYZ):
    #Run Kabsch algorithm on the two sets of points
    #construct the two matrices with the points

def getFileType(path, fileType):
    for fileName in os.listdir(path):
        if fileName.endswith(fileType):
            return os.path.join(path, fileName)

#m
def findYaw():
    parser = argparse.ArgumentParser(
        description="get the yaw of the camera"
    )
    parser.add_argument(
        "-f", "--folder", help="Folder with FAA data and csv", required=True
    )
    args = parser.parse_args()

    dataFile = args.folder
    FAAPath = os.path.join(dataFile, "imgtxt")
    csvFile = getFileType(dataFile, "label.csv")

    realXYZ, startTime = getRealXYZ(FAAPath)
    camXYZ = getCamXYZ(csvFile)
    yaw = findDiff(realXYZ, camXYZ)
    return yaw

################################################################################

#MATLAB Code

################################################################################
'''
%assume already in the appropriate folder

function data = extractFile(fileName, startRow, endRow)
    data = uiimport(fileName);
    data = data(startRow:endRow, :);
end

function [yaw] = findYaw()
    %my attempt to find the txt files and shove them into a cell structure
    numFiles = numel(dir(*file*.txt));
    startRow = 2;
    endRow = 6;
    FAAData = cell(1, numFiles);
    for num = 1:numFiles
        fileName = sprintf('imgtxt/file%04d.txt', num);
        FAAData{fileNum} = extractFile(fileName,startRow, endRow);
    end

    %my attempt to get csv data
    csvName = dir(*label*.csv);
    camData = uiimport(csvName(1));
    camData = camData(2:end,:);

    camXYZ, frames = getCamXYZ(camData);
    realXYZ = getRealXYZ(FAAData, frames);
    yaw = getDiff(realXYZ, camXYZ);
end


function [realXYZ] = getRealXYZ(FAAData, frames)
    
end

function [camXYZ, frames] = getCamXYZ(camData)
    xMin = camData(:,6);
    yMin = camData(:,7);
    xMax = camData(:,8);
    yMax = camData(:,9);
    frames = camData(:,1);
    frames = frames.erase(".png");
    camXY = [(xMin + xMax)/2,(yMin+yMax)/2];
    camXYZ = cam2World(camXY);
end
'''