import os
import os.path
import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

#dataFolder = os.path.join(os.pardir,"09-06-20")
#dataFolder = os.path.join(dataFolder, "1")
#dataFolder = os.path.join(dataFolder, 'cam1')
#print(dataFolder)

def interpolate_labels(dataFolder): 

    file_list = os.listdir(dataFolder)
    file_list.sort(key=natural_keys)
    #print(file_list)

    prev_num = 0
    prev_name = ""
    max_diff = 101
    for file in file_list:
        if file.endswith(".label"):
            file_name = os.path.join(dataFolder,file)
            file_num = int(os.path.splitext(file)[0])
            file1 = open(file_name, 'r')
            labels1 = file1.readlines()
            for label in labels1:
                label_strip = label.split(' ')
                label_name = label.split(' ')[4].strip()
                if len(label_name) < 2:
                    if int(prev_num) != 0:
                        num_diff = file_num-prev_num
                        if num_diff > 1 and num_diff < max_diff:
                            file2 = open(prev_name,'r')
                            labels2 = file2.readlines()
                            for label2 in labels2:
                                label2_name = label2.split(' ')[4].strip()
                                if label2_name == label_name:
                                    label1_strip = label.split(' ')
                                    label2_strip = label2.split(' ')
                                    x1 = int(label1_strip[0])
                                    y1 = int(label1_strip[1])
                                    x2 = int(label1_strip[2])
                                    y2 = int(label1_strip[3])
                                    x1_2 = int(label2_strip[0])
                                    y1_2 = int(label2_strip[1])
                                    x2_2 = int(label2_strip[2])
                                    y2_2 = int(label2_strip[3])
                                    frac = 1.0/num_diff
                                    for n in range(1,num_diff):
                                        newx1 = int((x1-x1_2)*frac*n) + x1_2
                                        newy1 = int((y1-y1_2)*frac*n) + y1_2
                                        newx2 = int((x2-x2_2)*frac*n) + x2_2
                                        newy2 = int((y2-y2_2)*frac*n) + y2_2
                                        print('New '+ os.path.join(dataFolder,str(prev_num+n) + '.label'))
                                        newfile = open(os.path.join(dataFolder,str(prev_num+n) + '.label'), 'a')
                                        newfile.write(str(newx1) + ' ' + str(newy1) + ' ' + str(newx2) + ' ' + str(newy2) + ' ' + label_name +  '\n')
                                        newfile.close()
                        
                            file2.close()
                        prev_num = int(file_num)
                        prev_name = file_name
                    else:
                        print('Found first hand label at ' + str(file_num))
                        prev_num = int(file_num)
                        print(prev_num)
                        prev_name = file_name
            file1.close()
            



