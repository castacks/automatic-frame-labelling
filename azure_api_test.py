import os, uuid, time
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient, __version__

# t = time.time()
# connect_str = os.getenv('AZURE_STORAGE')
# blob_service_client = BlobServiceClient.from_connection_string(connect_str)
# container_name = "inflight-garmin-data"
# local_file_name = "processed/1/2.png"
# print(t-time.time())
# blob_client = blob_service_client.get_blob_client(container=container_name, blob=local_file_name)
# # data = blob_client.download_blob() 

# with open("test.png", "wb") as download_file:
#     download_file.write(blob_client.download_blob().readall())


def get_azure(container_name,local_file_name):
    connect_str = os.getenv('AZURE_STORAGE')
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)
    blob_client = blob_service_client.get_blob_client(container=container_name, blob=local_file_name)
    data = blob_client.download_blob()

    return data 

if __name__ == "__main__":
    container_name = "inflight-garmin-data"
    local_file_name = "processed/1/2.png"
    data = get_azure(container_name,local_file_name)