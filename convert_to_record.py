# pylint: disable=C

import pandas as pd
import os
import io
import argparse
import tensorflow as tf
import contextlib2
import cv2
from collections import namedtuple
from PIL import Image
from object_detection.utils import dataset_util
from object_detection.dataset_tools import tf_record_creation_util

parser = argparse.ArgumentParser(description='Converter from CSV to TFRecord files')
parser.add_argument('-f', '--file', help='CSV File that should be converted', required=True)
parser.add_argument('-s', '--shards', help='Number of shards to split the output into', required=True)
args = parser.parse_args()

num_shards = int(args.shards)
output_filebase = '.'.join(args.file.split('.')[:-1] + ['tfrecord'])

with contextlib2.ExitStack() as tf_record_close_stack:
    # writer = tf_record_creation_util.open_sharded_output_tfrecords(tf_record_close_stack, output_filebase, num_shards)
    writer = tf.io.TFRecordWriter(output_filebase)
    
    image_path = os.path.dirname(args.file)
    
    csv_data = pd.read_csv(args.file)
    csv_grouped = csv_data.groupby('filename')
    
    tuple_frame = namedtuple('data', ['filename', 'object'])
    csv_grouped = [tuple_frame(filename, csv_grouped.get_group(group)) for filename, group in zip(csv_grouped.groups.keys(), csv_grouped.groups)]
    
    for idx, group in enumerate(csv_grouped):
        with tf.io.gfile.GFile(os.path.join(image_path, '{}'.format(group.filename)), 'rb') as gfile:
            encoded_img = gfile.read()
        
        encoded_img_io = io.BytesIO(encoded_img)
        image = Image.open(encoded_img_io)
        width, height = image.size
        
        filename = group.filename.encode('utf8')
        image_format = b'png'
        
        xmins = []
        xmaxs = []
        ymins = []
        ymaxs = []
        classes_text = []
        classes = []
        
        for index, row in group.object.iterrows():
            xmins.append(row['xmin'] / width)
            xmaxs.append(row['xmax'] / width)
            ymins.append(row['ymin'] / height)
            ymaxs.append(row['ymax'] / height)
            # Hardcoded values!!!
            classes_text.append(row["class"].encode('utf-8'))
            classes.append(6)
            
        if len(classes) != len(xmins) or len(xmaxs) != len(xmins) or len(ymins) != len(xmaxs) or len(ymaxs) != len(ymins) or len(classes_text) != len(ymaxs):
            print("ERROR: Lengths not matching!")
            break
        
        tf_example = tf.train.Example(features=tf.train.Features(feature={
            'image/height': dataset_util.int64_feature(height),
            'image/width': dataset_util.int64_feature(width),
            'image/filename': dataset_util.bytes_feature(filename),
            'image/source_id': dataset_util.bytes_feature(filename),
            'image/encoded': dataset_util.bytes_feature(encoded_img),
            'image/format': dataset_util.bytes_feature(image_format),
            'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
            'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
            'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
            'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
            'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
            'image/object/class/label': dataset_util.int64_list_feature(classes),
        }))
        
        # output_shard_index = idx % num_shards
        # writer[output_shard_index].write(tf_example.SerializeToString())
        
        writer.write(tf_example.SerializeToString())