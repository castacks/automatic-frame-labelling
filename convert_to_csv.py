#!/usr/bin/python3

import os
import pandas as pd
import argparse
import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser(description='Converts XML files to a CSV file')
parser.add_argument('-f', '--folder', help='Folder where the XML files are stored')
args = parser.parse_args()

xml_list = []
for f in os.listdir(args.folder):
    if f.endswith('.xml'):
        tree = ET.parse(os.path.join(args.folder, f))
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text,
                    int(root.find('size')[0].text),
                    int(root.find('size')[1].text),
                    member[0].text,
                    int(member[4][0].text),
                    int(member[4][1].text),
                    int(member[4][2].text),
                    int(member[4][3].text)
                    )
            xml_list.append(value)

column_names = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
xml_df = pd.DataFrame(xml_list, columns=column_names)
print(args.folder.split('\\')[-1] + '_label.csv')
print(args.folder)
xmlPath = os.path.join(args.folder, args.folder.split('\\')[-1] + '_label.csv')
print(xmlPath)
xml_df.to_csv(xmlPath)