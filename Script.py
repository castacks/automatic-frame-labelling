from geographiclib.geodesic import Geodesic
from datetime import datetime
import datetime
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import math
import argparse
from tkinter import *

################################################################################

#Helper Functions

################################################################################

def getFileType(path, fileType):
    for fileName in os.listdir(path):
        if fileName.endswith(fileType):
            return os.path.join(path, fileName)

def readFile(path):
    with open(path, "rt") as f:
        return f.read()
    pass

def subtractTime(t1, t2):
    (h1, m1, s1) = t1
    (h2, m2, s2) = t2
    return (h1 - h2) * 3600 + (m1 - m2) * 60 + (s1 - s2)

def addTime(t1, t2):
    (h1, m1, s1) = t1
    (h2, m2, s2) = t2
    s = (s1 + s2) % 60
    scarry = math.floor((s1 + s2) / 60)
    m = (m1 + m2 + scarry) % 60
    mcarry = math.floor((m1 + m2 + scarry) / 60)
    h = (h1 + h2 + mcarry)
    return (h, m, s)

def displace(coords, dist, tangent):
    lat = coords[0]
    lon = coords[1]
    geod = Geodesic.WGS84
    g = geod.Direct(lat, lon, tangent, dist * 1000)
    return (g['lat2'], g['lon2'])

#gets range and bearing given two coords
def get_range_and_bearing(lat1,lon1,lat2,lon2):
    geod= Geodesic.WGS84
    lat2=float(lat2)
    lon2=float(lon2)
    g = geod.Inverse(lat1,lon1,lat2,lon2)
    return g['s12']/1000.0, g['azi1']

def timeAfter(t1, t2):
    return subtractTime(t1, t2) > 0

def timeBefore(t1, t2):
    return subtractTime(t1, t2) < 0

def getStartTime(lines):
    return lines[1].split(" ")

def getEndTime(lines):
    return lines[-2].split(" ")

def getPressure(lines): #pulls inHg pressure from METAR string
    pressure = -1
    for line in lines:
        if line.find("METAR") != -1:
            metarString = line.split(" ")
            for i in range(6,11):
                pressure = metarString[i] #8
                if pressure[0] == "A":
                    print(pressure)
                    assert(isValidPressure(pressure))
                    pressure = int(pressure[1:])
                    return pressure
    return pressure

#correctness and safety of metar string extraction
def isValidPressure(pressure): #should look like "A1234"
    assert(pressure.isalnum())
    assert(pressure[0] == "A")
    assert(pressure[1:].isnumeric())
    assert(2600 < int(pressure[1:]) < 3203) #make sure pressure makes sense
    return True

#Using the NOAA's formula for pressure altitude
#alt = 145,366.45(1 - (x inHg/29.92)^0.190284)
#Adjusts a plane's reported altitude for pressure correction
def adjustAlt(altimeterSetting, alt):
    pressureReading = 2992*((1 - alt / 145366.45) ** (1/0.190284))
    if altimeterSetting == -1: 
        #if we cannot find valid pressure from METAR string, simply use default
        altimeterSetting = 2992
    accurateAlt = 145366.45 * (1 - (pressureReading/altimeterSetting) ** 0.190284)
    return accurateAlt

################################################################################
#get the csvData, clean for empty lines. Result is a list of lines. Each line in
#string format
def getCSVData(csvPath):
    csvData=[]
    read = readFile(os.path.normpath(csvPath))
    split=read.split("\n")
    for line in split:
        for character in line:
            if character.isalnum(): #clean off empty lines
                csvData.append(line) #final is a list of strings. each string is a line
                break
    csvData.pop(0) #take off column headers
    return csvData

def cleanCSVLine(line):
    plane = line[15]
        
    line[3]=line[3].replace("'","") #c[3] is seconds
    line[3]=line[3].replace("]","")
    line[3]=line[3].replace('"',"")
    line[3]=line[3].strip()
    sec = line[3].replace("u","") 
    second = float(sec)
    line[2]=line[2].replace("'","") #c[3] is min
    line[2]=line[2].replace('"',"")
    line[2]=line[2].strip()
    mint = line[2].replace("u","")
    minute = int(mint)
    line[1]=line[1].replace("'","") #c[1] is hr
    line[1]=line[1].replace("[","")
    line[1]=line[1].replace('"',"")
    line[1]=line[1].strip()
    hr = line[1].replace("u","")
    hour = int(hr)

    x = float(line[10])
    y = float(line[11])
    z = float(line[7])
    return (plane, second, hour, minute, x, y, z)

def updateADSBDict(x, y, z, hour, minute, second, plane, result):
    #if the plane is not in the dictionary, add a list in the dictionary 
    # with the plane ID as the key.
    if (plane not in result):#if (i == 0):
        result[plane] = ([(hour, minute, second)], {(hour, minute, second):\
                            [x, y, z]})
    #if the plane is in the dictionary, append the new time and position to
    # the dictionary #else:
    else: 
        if (hour, minute, second) not in result[plane][0]:
            result[plane][0].append((hour, minute, second))
            result[plane][1][(hour, minute, second)] =  [x, y, z]
    return result

#Produces a dictionary with ADS-B Data
#Key is the string of the Mode 2 Hex Code of the plane, labeled ID in the csv
#Value is a tuple with a list and a dictionary.
#List contains the sorted list of ADS-B times.
#Dictionary contains the lat, lon, alt indexed to those times
def extractInfo(csvFile, resultsPath, pressure):
    result = dict()#results dictionary
    csvData = getCSVData(csvFile)#pull data from the .csv file
    f = open(os.path.normpath(os.path.join(resultsPath, "General.txt")), "w+")

    #loop through the csvFile
    max = len(csvData)
    for i in range(0,max):
        line = csvData[i].split(',') #c is a list of the items in every row
        plane, second, hour, minute, x, y, z = cleanCSVLine(line)
        
        z = adjustAlt(pressure, z)
        
        result = updateADSBDict(x, y, z, hour, minute, second, plane, result)
        #update the dict of 3D points

        #put the relevant info into the general txt file at the start and end
        if i == max - 1 or i == 0:
            if i == 0:
                f.write(f"Start time:\n%d:%d:%f\n"% (hour, minute, second))
            elif i == max - 1:
                f.write(f"End time:\n%d:%d:%f\n" % (hour, minute, second))
                f.write(f"Start location lat-long-alt{x, y, z} \n")
                f.close()
    return result

def parseTimeList(timeList):
    startTime = dateTimeStart[1].split(":") #list with [hour,minute,second]
    startH = int(startTime[0]) #hours in decimal
    startM = int(startTime[1]) #minutes in decimal
    startS = float(startTime[2]) #seconds in decimal
    start = (startH,startM,startS)
    return start

#gets a list of all the time intervals of the frames, as well as date of recording
def getTimes(txtFile, videoFile, tInterval, camDelay):
    rawFile = readFile(os.path.normpath(txtFile))
    lines = rawFile.split("\n")
    dateTimeStart = getStartTime(lines)
    dateTimeEnd = getEndTime(lines)
    pressureSetting = getPressure(lines)
    if pressureSetting == -1:
        print("Pressure invalid")
        pressure = 2992 #if the pressure is not properly extracted, go with default

    cap = cv2.VideoCapture(os.path.normpath(videoFile))
    fps = cap.get(cv2.CAP_PROP_FPS)     #29.97
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count/fps 
    frame_count = int(duration / tInterval) 
    print(f"Frames: {frame_count}")

    #recording start time, according to txt file
    startTime = dateTimeStart[1].split(":") #list with [hour,minute,second]
    startH = int(startTime[0]) #hours in decimal
    startM = int(startTime[1]) #minutes in decimal
    startS = float(startTime[2]) #seconds in decimal
    start = (startH,startM,startS)
    print("Official Start Time, according to txt: %f" %startS)    
    endTime = dateTimeEnd[1].split(":") #list with [hour,minute,second]
    endH = int(endTime[0]) #hours in decimal
    endM = int(endTime[1]) #minutes in decimal
    endS = float(endTime[2]) #seconds in decimal
    end = (endH, endM, endS)

    #we approximate the actual start by assuming that the start and stop delay are the same
    officialLength = subtractTime(end, start) #interval between "start" and "end" time of the recording
    actualLength = duration #actual length of video
    error = officialLength - actualLength
    print("Official Length of Video: %f Actual Length of Video:%f" %(officialLength, actualLength))
    (hTime, mTime, sTime) = addTime(start, (0,0,camDelay)) #adjusted start time
    
    print("Start time we are using: %f" %sTime)
    
    dateList = dateTimeStart[0].split("-") #date list [year, month, day]
    sec = startTime[2].split(".") #second with [second, decimal of second]
    date = datetime.datetime(int(dateList[0]),int(dateList[1]),int(dateList[2]),
                             int(startTime[0]),int(startTime[1]),
                             int(sec[0]),int(sec[1]))
    times=[]

    for i in range(0,frame_count):
        second = (sTime+(i*tInterval)) % 60
        minCarry = int((sTime+(i*tInterval)) / 60)
        minute = (mTime + minCarry) % 60
        hrCarry = int((mTime + minCarry) / 60)
        hour = hTime + hrCarry
        times.append((hour, minute, second))

    return times,date,pressureSetting

#Constants:
#A is the state transition model
#B is the control input model(unused)
#H is the observation model
#Inputs:
#P is the state convariance matrix
#Unknowns:
#Q is the process noise covariance matrix
#K is the Kalman Gain
#R is the measurement covariance matrix
def kalmanStep(prevState, prevCov, measurement, Q, R, dt):
    #Need Q, R   
    #Prediction
    A = np.array([[1, dt], [0, 1]])
    B = np.array([[dt ** 2 / 2, dt]])
    X_k0 = A @ prevState # + B @ accel but what is accel? 
    P_k0 = A @ prevCov @ A.transpose() + Q #process covariation noise?

    #Update
    #measurement is current updated alt
    H = np.array([[1, 0]])
    preFitResid = measurement - H @ X_k0 #1 x 1 matrix,measurement has one value
    prefitCovar = H @ P_k0 @ H.transpose() + R # 1 x 1 matrix
    K = P_k0 @ H.transpose() @ np.linalg.inv(prefitCovar) 
    updatedState = X_k0 + K @ preFitResid
    updatedCovar = (np.eye(2) - K @ H) @ P_k0
    postfitResid = measurement - H @ updatedState

    return (updatedState, updatedCovar)

#gets a dictionary and list with the times of position changes, and the speeds
# before that position change occurs
def getSpeeds(times, positions):
    horizSpeeds = dict()
    hTimeStamps = []
    prevTime = times[0]

    vertSpeeds = dict()
    vTimeStamps = []
    prevAltTime = times[0]

    prevLat, prevLon, prevAlt = positions[prevTime]
    #prevAlt = adjustAlt(pressure, prevAlt)
    #positions[prevTime] = [prevLat, prevLon, prevAlt]
    length = len(times)

    #Kalman Filter Params
    kalmanAlt = np.array([[prevAlt],
                          [0      ]])

    covar = np.array([[100, 0], [0, 60]])
    measError = np.array([825])
    processNoise = np.array([[1, 1],
                             [1, 1]])

    #for each timestamp in the FAA Dataset
    for i in range(1, length):
        time = times[i]
        lat, lon, alt = positions[time]
        
        #if the position has changed...
        if (lat != prevLat) or (lon != prevLon):
            #compute the speeds for this time period and add to dictionary
            dt = subtractTime(time, prevTime)
            latSpeed = (lat - prevLat) / dt
            lonSpeed = (lon - prevLon) / dt
            
            horizSpeeds[time] = (latSpeed, lonSpeed)
            #add the key to the list
            hTimeStamps.append(time)
            
            #update the prev for the next position change
            prevLat = lat
            prevLon = lon
            prevTime = time
        
        if (alt != prevAlt):
            dtAlt = subtractTime(time, prevAltTime)
            
            '''
            #Uncomment to use Kalman filter
            kalmanAlt, covar = kalmanStep(kalmanAlt, covar, alt, processNoise, measError, dtAlt)
            alt = kalmanAlt[0,0]
            '''
            altSpeed = (alt - prevAlt) / dtAlt
            vertSpeeds[time] = altSpeed
            vTimeStamps.append(time)
            prevAltTime = time
            prevAlt = alt

    return hTimeStamps, vTimeStamps, horizSpeeds, vertSpeeds

#produces txt files with the position of the aircraft for each timestamp
def produce(frameTimes, date, ADSBData, resultsPath,
            camLocation, camHeight, tInterval):
    fileNew = True
    points = dict()
    frame_count = len(frameTimes)
    aircraftList = open(os.path.normpath(os.path.join(
                        resultsPath, "Aircraft.txt")), "w+")
    for aircraft in ADSBData:
        aircraftList.write(aircraft + "\n")
        (FAATimes, FAAPositions) = ADSBData[aircraft]
        altitudeList = [FAAPositions[time][2] for time in FAATimes]
        #get the averages speeds between all position changes
        hChangeTimes, vChangeTimes, hSpeeds, vSpeeds \
            = getSpeeds(FAATimes, FAAPositions)
        newAltList = [FAAPositions[time][2] for time in FAATimes]
        
        #plot the Kalman adjustment. Comment out in use to avoid pop-up plots
        # fig, ax = plt.subplots()
        # x = np.linspace(0,len(altitudeList),len(altitudeList))
        # ax.plot(x, altitudeList)
        # ax.plot(x, newAltList)
        # plt.show()

        k = 0
        j = 0
        hSpeedTime = hChangeTimes[k]
        vSpeedTime = vChangeTimes[j]

        #For each frame...
        for i in range(0,frame_count):
            #find the latest FAA timestamp that's earlier than the current frame
            time = None
            frameTime = frameTimes[i]
            for FAATime in FAATimes:
                if timeBefore(FAATime, frameTime):
                    time = FAATime
            
            #if the frame appears earlier than the aircraft appears in ADS-B
            # data, use the first timestamp
            if time == None:
                time = FAATimes[0]

            #if we surpass the current speed interval, move on to the next
            if not timeBefore(frameTime, hSpeedTime):
                k += 1
                if k >= len(hChangeTimes):
                    k = len(hChangeTimes) - 1
                hSpeedTime = hChangeTimes[k]

            if not timeBefore(frameTime, vSpeedTime):
                j += 1
                if j >= len(vChangeTimes):
                    j = len(vChangeTimes) - 1
                vSpeedTime = vChangeTimes[j]
            
            #get the "last known position"
            lat = FAAPositions[hSpeedTime][0]
            lon = FAAPositions[hSpeedTime][1]
            alt = FAAPositions[vSpeedTime][2]
            #get the avg speed for this interval
            latSpeed, lonSpeed = hSpeeds[hSpeedTime]
            altSpeed = vSpeeds[vSpeedTime]
            dt = subtractTime(frameTime, hSpeedTime)
            dtAlt = subtractTime(frameTime, vSpeedTime)

            latn = lat + dt * latSpeed
            lonn = lon + dt * lonSpeed
            alt = alt + dtAlt * altSpeed

            #rewrite the txt files when code is rerun
            if fileNew:
                permission = "w+"
            else: #append each plane's positions in succession
                permission = "a"
            f = open(os.path.normpath(os.path.join(resultsPath,
                                      "file{}.txt".format(i + 1))), permission)

            ran,bear = get_range_and_bearing(camLocation[0], camLocation[1],
                                             latn, lonn)
            x = ran * math.sin(math.radians(bear))
            y = ran * math.cos(math.radians(bear))
            z = (alt*0.3048-camHeight)/1000
            
            #points is a dictionary containing all txt file contents, keys are
            #aircraft, values are a 3xF matrix with all the positions
            if aircraft in points:
                points[aircraft] = np.concatenate((points[aircraft], 
                                                   np.array([[x], [y], [z]])),
                                                   axis = 1)
            else:
                points[aircraft] = np.array([[x],[y],[z]])
            f.write(f"%-10f %-10f %-10f\n" \
                    % (x,y,z))
            f.close()
            
            date = date + datetime.timedelta(0,tInterval)
        fileNew = False
    aircraftList.close()
    return points

################################################################################

#Plotting Code

################################################################################
def drawLine(canvas, width, height, p1, p2, magnif):
    x1 = p1[0]
    y1 = p1[1]
    z1 = p1[2]
    x2 = p2[0]
    y2 = p2[1]
    z3 = p2[2]

    xa = width/2 + x1 * magnif
    xb = width/2 + x2 * magnif
    ya = height/2 - y1 * magnif
    yb = height/2 - y2 * magnif
    canvas.create_line( xa, ya, xb, yb, fill = "red")

def drawWaypoint(canvas, width, height, p, magnif, i, aircraft):
    x = p[0]
    y = p[1]
    z = p[2]
    r = 2
    xc = width/2 + x * magnif
    yc = height/2 - y * magnif
    x1 = xc - r
    y1 = yc - r
    x2 = xc + r
    y2 = yc + r

    canvas.create_oval( x1, y1, x2, y2, fill = "red")
    timestamp = "%s:%d s" % (aircraft,(i/10))
    canvas.create_text(xc + r, yc - r, text= timestamp, font= "Arial 8")

def drawCopter(canvas, width, height, p, magnif):
    xf = p[0]
    yf = p[1]
    zf = p[2]
    xf = xf * magnif + width/2 
    yf = -yf * magnif + height/2
    rad = 10
    canvas.create_oval(xf - rad, yf - rad, xf + rad, yf + rad, fill = "red")

def draw(canvas, width, height, dictionary):
    magnif = 200
    xMarkers = 32
    yMarkers = 20
    canvas.create_line(width/2, 0, width/2 , height)
    canvas.create_line(0, height/2, width, height/2)
    for aircraft in dictionary:
        points = dictionary[aircraft]
        length = points.shape[1] #number of columns in array
        for i in range(1, length):
            drawLine(canvas, width, height, points[:,i-1], points[:,i], magnif)
            if i % 200 == 0:
                drawWaypoint(canvas, width, height, points[:,i], magnif, i, aircraft)
        for i in range(xMarkers):
            dx = width/xMarkers
            h = height / 2
            w = dx * i
            d = 10
            canvas.create_line(w, h-d, w, h+d)
            canvas.create_text(w, h + d * 2, text=str((w - width/2)/magnif))

        for i in range(yMarkers):
            dy = height/yMarkers
            h = dy * i
            w = width /2
            d = 10
            canvas.create_line(w-d, h, w+d, h)
            canvas.create_text(w + d*2, h, text=str((-h + height/2)/magnif))

        canvas.create_text(width - 30, height/2 + 40, text = "x (km)")
        canvas.create_text(width / 2 + 50, 20, text = "y (km)")

        drawCopter(canvas, width, height, points[:,length -1], magnif)
    
#Code from 15-112 website 
# http://www.kosbie.net/cmu/fall-19/15-112/notes/notes-graphics-part1.html
def runDrawing(points, width=600, height=600):
    root = Tk()
    root.resizable(width=False, height=False) # prevents resizing window
    canvas = Canvas(root, width=width, height=height)
    canvas.configure(bd=0, highlightthickness=0)
    canvas.pack()
    draw(canvas, width, height, points)
    root.mainloop()
    print("bye!")


def FrameCapture(path):
      
    vidObj = cv2.VideoCapture(path) 
  
    count = 0
  
    success = 1
    path='/Users/marko/Desktop/CMU/20S/Airlab/images'# Enter the desired path
    while success: 

        success, image = vidObj.read() 
        cv2.imwrite(os.path.join(path, "%d.jpg" % count), image) 
        count += 1

def linearizeADSBData(dataFile, camGPS, camHeight, frameRate):
    
    print(f"Coords used: {camGPS}")
    camDelay = 0
    tInterval= 1/frameRate

    #set up all the path variables
    resultsPath = os.path.join(dataFile, "imgtxt")
    if not os.path.isdir(resultsPath):
        os.mkdir(resultsPath)
    videoFile = getFileType(dataFile, ".MP4")
    txtFile = getFileType(dataFile, ".txt")
    csvFile = getFileType(dataFile, ".csv")

    #time intervals of the frames, date of recording, individual aircraft
    frameTimes,date,pressure = getTimes(txtFile, videoFile, tInterval, camDelay)
    #list of timestamps in seconds from the FAA data
    ADSBData = extractInfo(csvFile, resultsPath, pressure) 
    points = produce(frameTimes, date, ADSBData, resultsPath, camGPS, camHeight, tInterval)
    #runDrawing(points, 1600, 1000)
    return points


#When run alone, points dictionary is not used
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="get the aircraft positions and plot them"
    )
    parser.add_argument(
        "-f", "--folder", help="Folder containg the video, txt and csv", required=True
    )

    args = parser.parse_args()

    dataFile = args.folder
    
    frameRate = 10
    cameraHeight = 317 #375 #303
    camLocation = (40.430306, -79.924497)#(40.777886, -79.949893) #GPS of Jay's house
    linearizeADSBData(dataFile, camLocation, cameraHeight, frameRate)