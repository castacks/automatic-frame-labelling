#polynomial coefficients for the DIRECT mapping function (ocam_model.ss in MATLAB). These are used by cam2world

5 -5.776318e+02 0.000000e+00 4.047242e-04 -1.424101e-07 1.702610e-10 

#polynomial coefficients for the inverse mapping function (ocam_model.invpol in MATLAB). These are used by world2cam

14 1121.591853 842.460956 -12.861934 11.606658 154.100041 -15.754858 -76.031270 89.885591 71.024084 -63.894725 -50.463719 17.524247 21.786794 4.749658 

#center: "row" and "column", starting from 0 (C convention)

1280.279197 1259.599061

#affine parameters "c", "d", "e"

1.000084 -0.000008 0.000408

#image size: "height" and "width"

2496 2496

